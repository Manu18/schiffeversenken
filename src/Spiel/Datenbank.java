package Spiel;

import java.sql.*;
import java.util.Properties;

public class Datenbank {
    private String userName;
    private String password;
    private int portNumber;
    private String dbms;
    private String serverName;
    private String dbName;
    private Connection con;

    public Datenbank() {
        this.userName = "mamf18";
        this.password = "ManuelSchiffe";
        this.portNumber = 3306;
        this.dbms = "mysql";
        this.serverName = "localhost";
        this.dbName = "schiffeversenken";
    }

    public Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection conn = null;
        Properties connectionProps = new Properties();
        connectionProps.put("user", this.userName);
        connectionProps.put("password", this.password);

        if (this.dbms.equals("mysql")) {
            conn = DriverManager.getConnection(
                    "jdbc:" + this.dbms + "://" +
                            this.serverName +
                            ":" + this.portNumber + "/",
                    connectionProps);
        } else if (this.dbms.equals("derby")) {
            conn = DriverManager.getConnection(
                    "jdbc:" + this.dbms + ":" +
                            this.dbName +
                            ";create=true",
                    connectionProps);
        }
        System.out.println("Connected to database");
        this.con = conn;
        PreparedStatement preparedStmt = con.prepareStatement("USE " + dbName);
        preparedStmt.execute();
        return conn;
    }

    public static void erstelleDB() {
        /// eigentlich unnötig
    }

    public void speicherSpieler(Spieler s) throws SQLException {
        String query = "INSERT INTO `user` (`id`, `name`, `password`, `getroffeneSchiffe`, `schuesse`, `kartenID`) VALUES (?, ?, ?, ?, ?, ?)";
        PreparedStatement preparedStmt = this.con.prepareStatement(query);
        preparedStmt.setInt(1, s.getId());
        preparedStmt.setString(2, s.getName());
        preparedStmt.setString(3, s.getPasswort());
        preparedStmt.setInt(4, s.getGetroffeneSchiffe());
        preparedStmt.setInt(5, s.getSchuesse());
        preparedStmt.setInt(6, s.getKartenId());
        // gegnerID einfügen
        preparedStmt.execute();
        System.out.println("Spieler in DB geschrieben");
    }

    public void speicherKarte(Spieler s) throws SQLException {
        // schiffe
        String[][] export = s.getKarte().exportKarte();
        for (int i = 0; i < export.length; i++) {
            String query = "INSERT INTO `schiffe` (`x`, `y`, `horizontal`, `leben`, `typ`, `treffer`, `versenkt`, `position`, `positionTreffer`, `schiffID`, `kartenID`) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement preparedStmt = this.con.prepareStatement(query);
            preparedStmt.setInt(1, Integer.parseInt(export[i][0])); // x
            preparedStmt.setInt(2, Integer.parseInt(export[i][1])); // y
            preparedStmt.setBoolean(3, Boolean.parseBoolean(export[i][2])); // horizontal
            preparedStmt.setInt(4, Integer.parseInt(export[i][3])); // leben
            preparedStmt.setString(5, export[i][4]); // typ
            preparedStmt.setInt(6, Integer.parseInt(export[i][5])); // treffer
            preparedStmt.setBoolean(7, Boolean.parseBoolean(export[i][6])); // versenkt
            preparedStmt.setString(8, export[i][7]); // position
            preparedStmt.setString(9, export[i][8]); // treffer
            preparedStmt.setInt(10, Integer.parseInt(export[i][9])); // id Schiff
            preparedStmt.setInt(11, Integer.parseInt(export[i][10])); // id Karte
            preparedStmt.execute();
        }
        System.out.println("Karte in DB geschrieben");
    }

    public Schiffekarte importKarte(int karte) throws SQLException {
        Statement stmt = null;
        ResultSet result = null;
        String table = "schiffe";

        stmt = this.con.createStatement();
        result = stmt.executeQuery("SELECT * FROM " + table);
        result.first();

        Schiffekarte sk = new Schiffekarte(result.getInt(11));

        for (int i = 0; i < sk.schiffeSpieler.length; i++) {
            // Eigenschaften Schiff
            int x = result.getInt(1);
            int y = result.getInt(2);
            Boolean horizontal = result.getBoolean(3);
            int leben = result.getInt(4);
            String typ = result.getString(5);
            int treffer = result.getInt(6);
            Boolean versenkt = result.getBoolean(7);
            String positionS = result.getString(8);
            String trefferS = result.getString(9);
            int schiffID = result.getInt(10);
            //int kartenID = result.getInt(11);
            // result.next();
            Schiffe s = new Schiffe(schiffID, typ, leben, x, y, horizontal, treffer, versenkt,positionS, trefferS);
            sk.schiffeSpieler[i] = s;
        }
        return sk;
    }

    public Spieler importSpieler() throws SQLException {
        Statement stmt;
        ResultSet result;
        String table = "user";

        stmt = this.con.createStatement();
        result = stmt.executeQuery("SELECT * FROM " + table);
        result.first();
        int id = result.getInt(1);
        String name = result.getString(2);
        String pw = result.getString(3);
        int schuesse = result.getInt(4);
        int treffer = result.getInt(5);
        int kartenID = result.getInt(6);

        return new Spieler(id, name, pw, schuesse, treffer, kartenID);
    }

    public int updateSpielerCounter() throws SQLException {
        Statement stmt;
        ResultSet result;
        String table = "user";

        stmt = this.con.createStatement();
        result = stmt.executeQuery("SELECT * FROM " + table);
        int id = -1;
        
        while(result.next()) {
            if (result.isLast()) {
                id = result.getInt(1);
            }
        }

        return id;
    }

    public int updateKartenCounter() throws SQLException {
        Statement stmt;
        ResultSet result;
        String table = "schiffe";

        stmt = this.con.createStatement();
        result = stmt.executeQuery("SELECT * FROM " + table);
        int id = -1;

        while(result.next()) {
            if (result.isLast()) {
                id = result.getInt(11);
            }
        }

        return id;
    }

    public void closeConn() throws SQLException {
        this.con = null;
        this.con.close();
    }

    public static void main(String[] args) {
        Datenbank db = new Datenbank();
        try {
            db.getConnection();
            //Spieler m = new Spieler("Manuel");
            //db.speicherSpieler(m);
            //m.getKarte().schiffeSpieler[0].setPos(null,null,0,0,false);

            //Spieler.schiffeSet(null, Color.WHITE,m,0,0,0,false);
            //db.speicherKarte(m);

            Spieler imp = db.importSpieler();
            System.out.println(imp);

            // verbindung schließen
            db.closeConn();

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}