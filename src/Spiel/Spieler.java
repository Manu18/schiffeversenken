package Spiel;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLException;

public class Spieler {
    private String name;
    private String passwort;
    private Schiffekarte karte;
    int getroffeneSchiffe;
    int schuesse;
    private int id;
    public static int idCounter = 0;

    public Spieler(String name, String pw) {
        Datenbank db = new Datenbank();
        int kartenID = 0;
        try {
            db.getConnection();
            idCounter = db.updateSpielerCounter();
            kartenID = db.updateKartenCounter();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        this.id = ++idCounter;
        this.getroffeneSchiffe = 0;
        this.schuesse = 0;
        this.name = name;
        this.passwort = pw;
        this.karte = new Schiffekarte(++kartenID);
    }

    public Spieler(int id, String name, String pw, int schuesse, int treffer, int kartenID) {
        this.id = id;
        this.getroffeneSchiffe = treffer;
        this.schuesse = schuesse;
        this.name = name;
        this.passwort = pw;
        this.karte = new Schiffekarte(++kartenID);
    }

    public static boolean schiffeSet(JButton[][] bt, Color c, Spieler s, int nr, int x, int y, boolean horizontal) {
        // 0-3 boote, 4-6 kreuzer, 7-8, zerstoerer, 9 flugzeugtraeger
        boolean b = s.getKarte().schiffeSpieler[nr].setPos(bt, c, x, y, horizontal);
        return b;
    }

    public void setKarte(int kartenID) {
        Datenbank db = new Datenbank();
        try {
            this.karte = db.importKarte(kartenID);
            db.closeConn();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public String toString() {
        String s = " Name: " + getName() + "\n ID: " + getId() + "\n Schüsse: " + getSchuesse() + "\n Treffer: " + getGetroffeneSchiffe();
        return s;
    }

    // Getter
    public String getName() {
        return name;
    }
    public String getPasswort() {
        return passwort;
    }
    public Schiffekarte getKarte() {
        return karte;
    }
    public int getGetroffeneSchiffe() {
        return getroffeneSchiffe;
    }
    public int getSchuesse() {
        return schuesse;
    }
    public int getId() {
        return id;
    }
    public int getKartenId() {
        return this.karte.kartenID;
    }

}