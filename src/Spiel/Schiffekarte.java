package Spiel;

import javax.swing.*;

public class Schiffekarte {
    static int maxfeldgroesse = 10;
    int kartenID;
    boolean[][] schuesse = new boolean[maxfeldgroesse][maxfeldgroesse];
    // schuesse[x][y] bei schuss wird true
    boolean[][] treffer = new boolean[maxfeldgroesse][maxfeldgroesse];
    // wenn schiff getroffen wurde
    public Schiffe[] schiffeSpieler = new Schiffe[10];

    public Schiffekarte(int kartenID) {
        this.kartenID = kartenID;
        Schiffe boot1 = new Schiffe("Boot");
        Schiffe boot2 = new Schiffe("Boot");
        Schiffe boot3 = new Schiffe("Boot");
        Schiffe boot4 = new Schiffe("Boot");
        schiffeSpieler[0] = boot1;
        schiffeSpieler[1] = boot2;
        schiffeSpieler[2] = boot3;
        schiffeSpieler[3] = boot4;

        Schiffe kreuzer1 = new Schiffe("Kreuzer");
        Schiffe kreuzer2 = new Schiffe("Kreuzer");
        Schiffe kreuzer3 = new Schiffe("Kreuzer");
        schiffeSpieler[4] = kreuzer1;
        schiffeSpieler[5] = kreuzer2;
        schiffeSpieler[6] = kreuzer3;

        Schiffe zerstoerer1 = new Schiffe("Zerstoerer");
        Schiffe zerstoerer2 = new Schiffe("Zerstoerer");
        schiffeSpieler[7] = zerstoerer1;
        schiffeSpieler[8] = zerstoerer2;

        Schiffe flugzeugtraeger1 = new Schiffe("Flugzeugtraeger");
        schiffeSpieler[9] = flugzeugtraeger1;
    }

    public boolean getroffen(int x, int y, JButton bt, Spieler s) {
        boolean b = false;
        s.schuesse++;
        schuesse[x][y] = true;
        for (Schiffe schiffe : schiffeSpieler) {
            if (schiffe.abschuss(x, y, bt, s)) {
                b = true;
                break;
            }
        }
        return b;
    }

    public String[][] exportKarte() {
        String[][] karte = new String[schiffeSpieler.length][schiffeSpieler[0].exportSchiff().length+1];
        for (int i = 0; i < this.schiffeSpieler.length; i++) {
            for (int j = 0; j < schiffeSpieler[i].exportSchiff().length; j++) {
                karte[i][j] = schiffeSpieler[i].exportSchiff()[j];
            }
            karte[i][schiffeSpieler[i].exportSchiff().length] = String.valueOf(kartenID);
        }
        return karte;
    }

}