package Spiel;

import GUI.Spiellayout;

import javax.swing.*;
import java.awt.*;

public class Schiffe {
    int leben;
    int x;
    int y;
    boolean horizontal;
    int treffer;
    int schiffID;
    static int counter;
    boolean versenkt;
    static int maxfeldgrosse = Schiffekarte.maxfeldgroesse;
    boolean[][][] position = new boolean[maxfeldgrosse][maxfeldgrosse][2];
    static boolean[][][] posiitionenSchiffe = new boolean[maxfeldgrosse][maxfeldgrosse][2];
    // position[x][y][0] = positon,
    // position[x][y][1] = getroffen = true
    String typ;
    JButton[] btSchiff;

    public Schiffe(String typ) {
        this.typ = typ;
        this.x = -1;
        this.y = -1;
        this.horizontal = false;
        this.treffer = 0;
        this.versenkt = false;
        this.schiffID = counter;
        counter++;

        switch (typ) {
            case "Zerstoerer": // 2 Stück
                this.leben = 3;
                break;
            case "Boot": // 4 stück
                this.leben = 1;
                break;
            case "Flugzeugtraeger": // 1 stück
                this.leben = 4;
                break;
            case "Kreuzer": // 3 stück
                this.leben = 2;
                break;
        }
        this.btSchiff = new JButton[leben];
    }

    public Schiffe(int schiffID, String typ, int leben, int x, int y, boolean horizontal, int treffer, boolean versenkt, String positionS, String trefferS) {
        this.typ = typ;
        this.leben = leben;
        this.x = x;
        this.y = y;
        this.horizontal = horizontal;
        this.treffer = treffer;
        this.versenkt = versenkt;
        this.schiffID = schiffID;
        this.importSchiff(positionS, trefferS);

        // buttons
        this.btSchiff = findButton();
    }

    private JButton[] findButton() {
        JButton[] bts = new JButton[leben];
        int counter = 0;
        for (int i = 0; i < this.position.length; i++) {
            for (int j = 0; j < this.position[0].length; j++) {
                if (this.position[i][j][0]) {
                    bts[counter] = Spiellayout.posButton[i][j];
                    counter++;
                }
            }
        }
        return bts;
    }

    public boolean abstand(int x, int y, boolean horizontal) {
        // abstands ueberpruefung
        boolean abstand = false;
        boolean besetzt = false;
        // wahr wenn der abstand zu klein oder bereits belegt

        for (int i = 0; i < leben; i++) {
            // besetzt
            if (posiitionenSchiffe[(x + i) % 10][y][0] && horizontal) {
                abstand = true;
            }
            if (posiitionenSchiffe[x][(y + i) % 10][0] && horizontal) {
                abstand = true;
            }

            // links
            if (horizontal && posiitionenSchiffe[(x - 1 + 10) % 10][y][0]) {
                abstand = true;
            }
            if (!horizontal && posiitionenSchiffe[(x - 1 + 10) % 10][(y + i) % 10][0]) {
                abstand = true;
            }

            // rechts
            if (horizontal && posiitionenSchiffe[(x + leben) % 10][y][0]) {
                abstand = true;
            }
            if(!horizontal && posiitionenSchiffe[(x + 1) % 10][(y + i) % 10][0]){
                abstand = true;
            }

            // unten
            if (horizontal && posiitionenSchiffe[((x + i) % 10)][(y + 1) % 10][0]){
                abstand = true;
            }
            if (!horizontal && posiitionenSchiffe[x][(y + leben) % 10][0]) {
                abstand = true;
            }

            //oben
            if (horizontal && posiitionenSchiffe[((x + i) % 10)][(y - 1 + 10) % 10][0]){
                abstand = true;
            }
            if (!horizontal && posiitionenSchiffe[x][(y - 1 + 10) % 10][0]) {
                abstand = true;
            }

            // ecke links oben
            if (posiitionenSchiffe[(x - 1 + 10) % 10][(y - 1 + 10) % 10][0]) {
                abstand = true;
            }

            // ecke links unten
            if (horizontal && posiitionenSchiffe[(x - 1 + 10) % 10][(y + 1) % 10][0]) {
                abstand = true;
            }
            if (!horizontal && posiitionenSchiffe[(x - 1 + 10) % 10][(y + leben) % 10][0]) {
                abstand = true;
            }

            // ecke rechts oben
            if (horizontal && posiitionenSchiffe[(x + leben) % 10][(y - 1 + 10) % 10][0]) {
                abstand = true;
            }
            if (!horizontal && posiitionenSchiffe[(x + 1) % 10][(y - 1 + 10) % 10][0]){
                abstand = true;
            }

            // ecke rechts unten
            if (horizontal && posiitionenSchiffe[(x + leben) % 10][(y + 1) % 10][0]){
                abstand = true;
            }
            if (!horizontal && posiitionenSchiffe[(x + 1) % 10][(y + leben) % 10][0]) {
                abstand = true;
            }
        }
        if (abstand) {
            System.out.println("Andere Position wählen! (1 Kasten abstand zu einem anderen)");
            return false;
        }
        if (besetzt) {
            System.out.println("Andere Position wählen! (es steht bereits ein Schiff auf dieser Position)");
            return false;
        }
        return true;
    }

    public boolean setPos(JButton[][] bt, Color c, int x, int y, boolean horizontal) {
        if (abstand(x,y,horizontal)) {
            this.x = x;
            this.y = y;
            this.horizontal = horizontal;
            if (horizontal) {
                for (int i = 0; i < leben; i++) {
                    position[(x + i) % 10][y][0] = true;
                    position[(x + i) % 10][y][1] = false;
                    posiitionenSchiffe[(x + i) % 10][y][0] = true;
                    posiitionenSchiffe[(x + i) % 10][y][1] = false;
                    bt[(x + i) % 10][y].setBackground(c);
                    bt[(x + i) % 10][y].setForeground(c);
                    bt[(x + i) % 10][y].setEnabled(false);
                    btSchiff[i] = bt[(x + i) % 10][y];
                    markierGesperrt(bt, x, y, Spiellayout.gesperrtColor, true, i, leben);
                }
                System.out.println(this.typ + " an Position: X: " + x + " Y: " + y + " horizontal gesetzt");
            } else {
                for (int i = 0; i < leben; i++) {
                    position[x][(y + i) % 10][0] = true;
                    position[x][(y + i) % 10][1] = false;
                    posiitionenSchiffe[x][(y + i) % 10][0] = true;
                    posiitionenSchiffe[x][(y + i) % 10][1] = false;
                    bt[x][(y + i) % 10].setBackground(c);
                    bt[x][(y + i) % 10].setForeground(c);
                    bt[x][(y + i) % 10].setEnabled(false);
                    btSchiff[i] = bt[x][(y + i) % 10];
                    markierGesperrt(bt, x, y, Spiellayout.gesperrtColor, false, i, leben);
                }
                System.out.println(this.typ + " an Position: X: " + x + " Y: " + y + " vertikal gesetzt");
            }
            return true;
        } else {
            return false;
        }
    }

    private void markierGesperrt(JButton[][] bt, int x, int y, Color m, boolean horizontal, int k, int leben) {
        // markiert die gesperrten Felder um ein Schiff
        // links
        if (horizontal) {
            bt[(x - 1 + 10) % 10][y].setBackground(m);
            bt[(x - 1 + 10) % 10][y].setForeground(m);
            bt[(x - 1 + 10) % 10][y].setEnabled(false);
            bt[(x - 1 + 10) % 10][y].setToolTipText("Abstand");
        }
        if (!horizontal) {
            bt[(x - 1 + 10) % 10][(y + k) % 10].setBackground(m);
            bt[(x - 1 + 10) % 10][(y + k) % 10].setForeground(m);
            bt[(x - 1 + 10) % 10][(y + k) % 10].setEnabled(false);
            bt[(x - 1 + 10) % 10][(y + k) % 10].setToolTipText("Abstand");
        }

        // rechts
        if (horizontal) {
            bt[(x + leben) % 10][y].setBackground(m);
            bt[(x + leben) % 10][y].setForeground(m);
            bt[(x + leben) % 10][y].setEnabled(false);
            bt[(x + leben) % 10][y].setToolTipText("Abstand");
        }
        if(!horizontal) {
            bt[(x + 1) % 10][(y + k) % 10].setBackground(m);
            bt[(x + 1) % 10][(y + k) % 10].setForeground(m);
            bt[(x + 1) % 10][(y + k) % 10].setEnabled(false);
            bt[(x + 1) % 10][(y + k) % 10].setToolTipText("Abstand");
        }

        // unten
        if (horizontal) {
            bt[((x + k) % 10)][(y + 1) % 10].setBackground(m);
            bt[((x + k) % 10)][(y + 1) % 10].setForeground(m);
            bt[((x + k) % 10)][(y + 1) % 10].setEnabled(false);
            bt[((x + k) % 10)][(y + 1) % 10].setToolTipText("Abstand");
        }
        if (!horizontal) {
            bt[x][(y + leben) % 10].setBackground(m);
            bt[x][(y + leben) % 10].setForeground(m);
            bt[x][(y + leben) % 10].setEnabled(false);
            bt[x][(y + leben) % 10].setToolTipText("Abstand");
        }

        //oben
        if (horizontal) {
            bt[((x + k) % 10)][(y - 1 + 10) % 10].setBackground(m);
            bt[((x + k) % 10)][(y - 1 + 10) % 10].setForeground(m);
            bt[((x + k) % 10)][(y - 1 + 10) % 10].setEnabled(false);
            bt[((x + k) % 10)][(y - 1 + 10) % 10].setToolTipText("Abstand");
        }
        if (!horizontal) {
            bt[x][(y - 1 + 10) % 10].setBackground(m);
            bt[x][(y - 1 + 10) % 10].setForeground(m);
            bt[x][(y - 1 + 10) % 10].setEnabled(false);
            bt[x][(y - 1 + 10) % 10].setToolTipText("Abstand");
        }

        // ecke links oben
        if (horizontal || !horizontal) {
            bt[(x - 1 + 10) % 10][(y - 1 + 10) % 10].setBackground(m);
            bt[(x - 1 + 10) % 10][(y - 1 + 10) % 10].setForeground(m);
            bt[(x - 1 + 10) % 10][(y - 1 + 10) % 10].setEnabled(false);
            bt[(x - 1 + 10) % 10][(y - 1 + 10) % 10].setToolTipText("Abstand");
        }

        // ecke links unten
        if (horizontal) {
            bt[(x - 1 + 10) % 10][(y + 1) % 10].setBackground(m);
            bt[(x - 1 + 10) % 10][(y + 1) % 10].setForeground(m);
            bt[(x - 1 + 10) % 10][(y + 1) % 10].setEnabled(false);
            bt[(x - 1 + 10) % 10][(y + 1) % 10].setToolTipText("Abstand");
        }
        if (!horizontal) {
            bt[(x - 1 + 10) % 10][(y + leben) % 10].setBackground(m);
            bt[(x - 1 + 10) % 10][(y + leben) % 10].setForeground(m);
            bt[(x - 1 + 10) % 10][(y + leben) % 10].setEnabled(false);
            bt[(x - 1 + 10) % 10][(y + leben) % 10].setToolTipText("Abstand");
        }

        // ecke rechts oben
        if (horizontal) {
            bt[(x + leben) % 10][(y - 1 + 10) % 10].setBackground(m);
            bt[(x + leben) % 10][(y - 1 + 10) % 10].setForeground(m);
            bt[(x + leben) % 10][(y - 1 + 10) % 10].setEnabled(false);
            bt[(x + leben) % 10][(y - 1 + 10) % 10].setToolTipText("Abstand");
        }
        if (!horizontal) {
            bt[(x + 1) % 10][(y - 1 + 10) % 10].setBackground(m);
            bt[(x + 1) % 10][(y - 1 + 10) % 10].setForeground(m);
            bt[(x + 1) % 10][(y - 1 + 10) % 10].setEnabled(false);
            bt[(x + 1) % 10][(y - 1 + 10) % 10].setToolTipText("Abstand");
        }

        // ecke rechts unten
        if (horizontal) {
            bt[(x + leben) % 10][(y + 1) % 10].setBackground(m);
            bt[(x + leben) % 10][(y + 1) % 10].setForeground(m);
            bt[(x + leben) % 10][(y + 1) % 10].setEnabled(false);
            bt[(x + leben) % 10][(y + 1) % 10].setToolTipText("Abstand");
        }
        if (!horizontal) {
            bt[(x + 1) % 10][(y + leben) % 10].setBackground(m);
            bt[(x + 1) % 10][(y + leben) % 10].setForeground(m);
            bt[(x + 1) % 10][(y + leben) % 10].setEnabled(false);
            bt[(x + 1) % 10][(y + leben) % 10].setToolTipText("Abstand");
        }
    }

    public static void markieren(JButton[][] bt, Color c, int schiff, int x, int y, boolean horizontal) {
        //Color oldcolor = bt[x][y].getBackground();
        if (horizontal) { // auf x achse
            switch (schiff) {
                case 0:
                    // boot
                    for (int i = 0; i < 1; i++) {
                        // farbe vom aktiven button
                        // über den rand hinaus
                        if (bt[(x+i)%10][y].isEnabled()) {
                            bt[(x + i) % 10][y].setBackground(c);
                            bt[(x + i) % 10][y].setForeground(c);
                        }
                    }
                    break;
                case 1:
                    // kreuzer
                    for (int i = 0; i < 2; i++) {
                        if (bt[(x+i)%10][y].isEnabled()) {
                            bt[(x + i) % 10][y].setBackground(c);
                            bt[(x + i) % 10][y].setForeground(c);
                        }
                    }
                    break;
                case 2:
                    // zerstoerer
                    for (int i = 0; i < 3; i++) {
                        if (bt[(x+i)%10][y].isEnabled()) {
                            bt[(x + i) % 10][y].setBackground(c);
                            bt[(x + i) % 10][y].setForeground(c);
                        }
                    }
                    break;
                case 3:
                    // flugzeugtraeger
                    for (int i = 0; i < 4; i++) {
                        if (bt[(x+i)%10][y].isEnabled()) {
                            bt[(x + i) % 10][y].setBackground(c);
                            bt[(x + i) % 10][y].setForeground(c);
                        }
                    }
                    break;
            }
        } else {
            switch (schiff) {
                case 0:
                    // boot
                    for (int i = 0; i < 1; i++) {
                        // farbe vom aktiven button
                        // über den rand hinaus
                        if (bt[x][(y + i) % 10].isEnabled()) {
                            bt[x][(y + i) % 10].setBackground(c);
                            bt[x][(y + i) % 10].setForeground(c);
                        }
                    }
                    break;
                case 1:
                    // kreuzer
                    for (int i = 0; i < 2; i++) {
                        if (bt[x][(y + i) % 10].isEnabled()) {
                            bt[x][(y + i) % 10].setBackground(c);
                            bt[x][(y + i) % 10].setForeground(c);
                        }
                    }
                    break;
                case 2:
                    // zerstoerer
                    for (int i = 0; i < 3; i++) {
                        if (bt[x][(y + i) % 10].isEnabled()) {
                            bt[x][(y + i) % 10].setBackground(c);
                            bt[x][(y + i) % 10].setForeground(c);
                        }
                    }
                    break;
                case 3:
                    // flugzeugtraeger
                    for (int i = 0; i < 4; i++) {
                        if (bt[x][(y + i) % 10].isEnabled()) {
                            bt[x][(y + i) % 10].setBackground(c);
                            bt[x][(y + i) % 10].setForeground(c);
                        }
                    }
                    break;
            }
        }
    }

    public boolean abschuss(int x, int y, JButton bt, Spieler s) {
        boolean b = false;
        if (position[x][y][0]) {
            //getroffen
            treffer++;
            b = true;
            position[x][y][1] = true;
            System.out.println(this.typ + " wurde getroffen");
            bt.setBackground(Spiellayout.trefferColor);
            bt.setForeground(Spiellayout.trefferColor);
            bt.setToolTipText("Treffer");
            if (leben == treffer) {
                // versenkt
                versenkt = true;
                this.versenkt();
                System.out.println(this.typ + " versenkt");
                s.getroffeneSchiffe++;
                // Spielende
                if (s.getroffeneSchiffe == 10) {
                    System.out.println(s.getName() + " hat gewonnen und alle Schiffe versenkt!");
                }
            }
        }
        return b;
    }

    public void versenkt() {
        for (int i = 0; i < leben; i++) {
            btSchiff[i].setBackground(Spiellayout.versenktColor);
            btSchiff[i].setForeground(Spiellayout.versenktColor);
            btSchiff[i].setToolTipText(this.typ + " versenkt");
        }

    }

    public String[] exportSchiff() {
        String[] export = new String[10];  // unklar bis jetzt
        export[0] = String.valueOf(x);
        export[1] = String.valueOf(y);
        export[2] = String.valueOf(horizontal);
        export[3] = String.valueOf(leben);
        export[4] = String.valueOf(typ);
        export[5] = String.valueOf(treffer);
        export[6] = String.valueOf(versenkt); // boolean!!

        // position
        String positionS = "";
        String trefferS = "";

        for (int i = 0; i < this.position.length; i++) {
            for (int j = 0; j < this.position[0].length; j++) {
                if (this.position[i][j][0]) {
                    positionS += "1";
                }
                if (!this.position[i][j][0]) {
                    positionS += "0";
                }
                if (this.position[i][j][1]) {
                    trefferS += "1";
                }
                if (!this.position[i][j][1]) {
                    trefferS += "0";
                }
            }
        }
        export[7] = positionS; // position[][][0] position
        export[8] = trefferS; // position[][][1] treffer
        export[9] = String.valueOf(schiffID);

        return export;
    }

    public void importSchiff(String positionS, String trefferS) {
        for (int i = 0; i < maxfeldgrosse; i++) {
            for (int j = 0; j < maxfeldgrosse; j++) {
                if (positionS.equals("1")) {
                    this.position[i][j][0] = true;
                }
                if (trefferS.equals("1")) {
                    this.position[i][j][1] = true;
                }
            }
        }
    }



}