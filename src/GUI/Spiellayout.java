package GUI;

import Spiel.Datenbank;
import Spiel.Schiffe;
import Spiel.Spieler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.sql.SQLException;

public class Spiellayout extends JFrame implements ActionListener {
    boolean auswahlmodus = true;
    private Spieler spieler;
    // private Spieler gegner;
    private Color oldcolor;
    public static Color trefferColor = Color.red;
    public static Color verfehltColor = Color.green;
    public static Color markierColor = Color.red;
    public static Color setzColor = Color.black;
    public static Color versenktColor = Color.white;
    public static Color gesperrtColor = Color.yellow;

    private int bootNr = 0;
    private int bootMax = 4; // 0 inkl. 4 exkl
    private int kreuzerNr = 4;
    private int kreuzerMax = 7;
    private int zerstoererNr = 7;
    private int zerstoerertMax = 9;
    private int flugzeugtraegerNr = 9;
    private int flugzeugtraegerMax = 10;


    private JButton bAbschuss;
    private JTextArea chatTextArea;
    private JButton b00;
    private JButton b10;
    private JButton b20;
    private JButton b30;
    private JButton b40;
    private JButton b50;
    private JButton b60;
    private JButton b70;
    private JButton b80;
    private JButton b90;
    private JButton b01;
    private JButton b11;
    private JButton b21;
    private JButton b02;
    private JButton b03;
    private JButton b04;
    private JButton b05;
    private JButton b06;
    private JButton b09;
    private JButton b08;
    private JButton b07;
    private JButton b74;
    private JButton b73;
    private JButton b72;
    private JButton b71;
    private JButton b82;
    private JButton b81;
    private JButton b91;
    private JButton b92;
    private JButton b83;
    private JButton b61;
    private JButton b62;
    private JButton b51;
    private JButton b75;
    private JButton b84;
    private JButton b85;
    private JButton b63;
    private JButton b64;
    private JButton b65;
    private JButton b52;
    private JButton b53;
    private JButton b55;
    private JButton b76;
    private JButton b66;
    private JButton b56;
    private JButton b41;
    private JButton b42;
    private JButton b43;
    private JButton b44;
    private JButton b45;
    private JButton b46;
    private JButton b67;
    private JButton b68;
    private JButton b77;
    private JButton b78;
    private JButton b12;
    private JButton b32;
    private JButton b24;
    private JButton b34;
    private JButton b23;
    private JButton b33;
    private JButton b22;
    private JButton b31;
    private JButton b35;
    private JButton b36;
    private JButton b47;
    private JButton b57;
    private JButton b58;
    private JButton b48;
    private JButton b37;
    private JButton b38;
    private JButton b25;
    private JButton b13;
    private JButton b14;
    private JButton b26;
    private JButton b15;
    private JButton b16;
    private JButton b27;
    private JButton b59;
    private JButton b69;
    private JButton b79;
    private JButton b49;
    private JButton b39;
    private JButton b28;
    private JButton b17;
    private JButton b18;
    private JButton b19;
    private JButton b29;
    private JButton b93;
    private JButton b94;
    private JButton b95;
    private JButton b96;
    private JButton b86;
    private JButton b87;
    private JButton b88;
    private JButton b89;
    private JButton b97;
    private JButton b98;
    private JButton b99;
    private JPanel jpanel;
    private JPanel chat;
    private JButton b54;
    private JPanel spielfeld;
    private JRadioButton rkreuzer;
    private JRadioButton rzerstoerer;
    private JRadioButton rflugzeugtraeger;
    private JRadioButton rboot;
    private JTextArea textArea1;
    private JButton bsetzen;
    private JCheckBox chorizontal;

    // button feld
    public static JButton[][] posButton = new JButton[10][10];

    public Spiellayout(Spieler spieler) {
        //UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
        // benötigt die fehlerbehandlung

        ButtonGroup gruppe = new ButtonGroup();
        //JRadioButtons werden zur ButtonGroup hinzugefügt
        gruppe.add(rkreuzer);
        gruppe.add(rzerstoerer);
        gruppe.add(rflugzeugtraeger);
        gruppe.add(rboot);

        oldcolor = b00.getBackground();

        this.spieler = spieler;
        //this.gegner = gegner;
        setContentPane(this.jpanel);
        setTitle("Schiffeversenken");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(800,500);
        setVisible(true);
        setLocationRelativeTo(null);

        posButton[0][0] = b00;
        posButton[0][1] = b01;
        posButton[0][2] = b02;
        posButton[0][3] = b03;
        posButton[0][4] = b04;
        posButton[0][5] = b05;
        posButton[0][6] = b06;
        posButton[0][7] = b07;
        posButton[0][8] = b08;
        posButton[0][9] = b09;
        posButton[1][0] = b10;
        posButton[1][1] = b11;
        posButton[1][2] = b12;
        posButton[1][3] = b13;
        posButton[1][4] = b14;
        posButton[1][5] = b15;
        posButton[1][6] = b16;
        posButton[1][7] = b17;
        posButton[1][8] = b18;
        posButton[1][9] = b19;
        posButton[2][0] = b20;
        posButton[2][1] = b21;
        posButton[2][2] = b22;
        posButton[2][3] = b23;
        posButton[2][4] = b24;
        posButton[2][5] = b25;
        posButton[2][6] = b26;
        posButton[2][7] = b27;
        posButton[2][8] = b28;
        posButton[2][9] = b29;
        posButton[3][0] = b30;
        posButton[3][1] = b31;
        posButton[3][2] = b32;
        posButton[3][3] = b33;
        posButton[3][4] = b34;
        posButton[3][5] = b35;
        posButton[3][6] = b36;
        posButton[3][7] = b37;
        posButton[3][8] = b38;
        posButton[3][9] = b39;
        posButton[4][0] = b40;
        posButton[4][1] = b41;
        posButton[4][2] = b42;
        posButton[4][3] = b43;
        posButton[4][4] = b44;
        posButton[4][5] = b45;
        posButton[4][6] = b46;
        posButton[4][7] = b47;
        posButton[4][8] = b48;
        posButton[4][9] = b49;
        posButton[5][0] = b50;
        posButton[5][1] = b51;
        posButton[5][2] = b52;
        posButton[5][3] = b53;
        posButton[5][4] = b54;
        posButton[5][5] = b55;
        posButton[5][6] = b56;
        posButton[5][7] = b57;
        posButton[5][8] = b58;
        posButton[5][9] = b59;
        posButton[6][0] = b60;
        posButton[6][1] = b61;
        posButton[6][2] = b62;
        posButton[6][3] = b63;
        posButton[6][4] = b64;
        posButton[6][5] = b65;
        posButton[6][6] = b66;
        posButton[6][7] = b67;
        posButton[6][8] = b68;
        posButton[6][9] = b69;
        posButton[7][0] = b70;
        posButton[7][1] = b71;
        posButton[7][2] = b72;
        posButton[7][3] = b73;
        posButton[7][4] = b74;
        posButton[7][5] = b75;
        posButton[7][6] = b76;
        posButton[7][7] = b77;
        posButton[7][8] = b78;
        posButton[7][9] = b79;
        posButton[8][0] = b80;
        posButton[8][1] = b81;
        posButton[8][2] = b82;
        posButton[8][3] = b83;
        posButton[8][4] = b84;
        posButton[8][5] = b85;
        posButton[8][6] = b86;
        posButton[8][7] = b87;
        posButton[8][8] = b88;
        posButton[8][9] = b89;
        posButton[9][0] = b90;
        posButton[9][1] = b91;
        posButton[9][2] = b92;
        posButton[9][3] = b93;
        posButton[9][4] = b94;
        posButton[9][5] = b95;
        posButton[9][6] = b96;
        posButton[9][7] = b97;
        posButton[9][8] = b98;
        posButton[9][9] = b99;

        b00.addActionListener((ActionListener) this);
        b01.addActionListener((ActionListener) this);
        b02.addActionListener((ActionListener) this);
        b03.addActionListener((ActionListener) this);
        b04.addActionListener((ActionListener) this);
        b05.addActionListener((ActionListener) this);
        b06.addActionListener((ActionListener) this);
        b07.addActionListener((ActionListener) this);
        b08.addActionListener((ActionListener) this);
        b09.addActionListener((ActionListener) this);
        b10.addActionListener((ActionListener) this);
        b11.addActionListener((ActionListener) this);
        b12.addActionListener((ActionListener) this);
        b13.addActionListener((ActionListener) this);
        b14.addActionListener((ActionListener) this);
        b15.addActionListener((ActionListener) this);
        b16.addActionListener((ActionListener) this);
        b17.addActionListener((ActionListener) this);
        b18.addActionListener((ActionListener) this);
        b19.addActionListener((ActionListener) this);
        b20.addActionListener((ActionListener) this);
        b21.addActionListener((ActionListener) this);
        b22.addActionListener((ActionListener) this);
        b23.addActionListener((ActionListener) this);
        b24.addActionListener((ActionListener) this);
        b25.addActionListener((ActionListener) this);
        b26.addActionListener((ActionListener) this);
        b27.addActionListener((ActionListener) this);
        b28.addActionListener((ActionListener) this);
        b29.addActionListener((ActionListener) this);
        b30.addActionListener((ActionListener) this);
        b31.addActionListener((ActionListener) this);
        b32.addActionListener((ActionListener) this);
        b33.addActionListener((ActionListener) this);
        b34.addActionListener((ActionListener) this);
        b35.addActionListener((ActionListener) this);
        b36.addActionListener((ActionListener) this);
        b37.addActionListener((ActionListener) this);
        b38.addActionListener((ActionListener) this);
        b39.addActionListener((ActionListener) this);
        b40.addActionListener((ActionListener) this);
        b41.addActionListener((ActionListener) this);
        b42.addActionListener((ActionListener) this);
        b43.addActionListener((ActionListener) this);
        b44.addActionListener((ActionListener) this);
        b45.addActionListener((ActionListener) this);
        b46.addActionListener((ActionListener) this);
        b47.addActionListener((ActionListener) this);
        b48.addActionListener((ActionListener) this);
        b49.addActionListener((ActionListener) this);
        b50.addActionListener((ActionListener) this);
        b51.addActionListener((ActionListener) this);
        b52.addActionListener((ActionListener) this);
        b53.addActionListener((ActionListener) this);
        b54.addActionListener((ActionListener) this);
        b55.addActionListener((ActionListener) this);
        b56.addActionListener((ActionListener) this);
        b57.addActionListener((ActionListener) this);
        b58.addActionListener((ActionListener) this);
        b59.addActionListener((ActionListener) this);
        b60.addActionListener((ActionListener) this);
        b61.addActionListener((ActionListener) this);
        b62.addActionListener((ActionListener) this);
        b63.addActionListener((ActionListener) this);
        b64.addActionListener((ActionListener) this);
        b65.addActionListener((ActionListener) this);
        b66.addActionListener((ActionListener) this);
        b67.addActionListener((ActionListener) this);
        b68.addActionListener((ActionListener) this);
        b69.addActionListener((ActionListener) this);
        b70.addActionListener((ActionListener) this);
        b71.addActionListener((ActionListener) this);
        b72.addActionListener((ActionListener) this);
        b73.addActionListener((ActionListener) this);
        b74.addActionListener((ActionListener) this);
        b75.addActionListener((ActionListener) this);
        b76.addActionListener((ActionListener) this);
        b77.addActionListener((ActionListener) this);
        b78.addActionListener((ActionListener) this);
        b79.addActionListener((ActionListener) this);
        b80.addActionListener((ActionListener) this);
        b81.addActionListener((ActionListener) this);
        b82.addActionListener((ActionListener) this);
        b83.addActionListener((ActionListener) this);
        b84.addActionListener((ActionListener) this);
        b85.addActionListener((ActionListener) this);
        b86.addActionListener((ActionListener) this);
        b87.addActionListener((ActionListener) this);
        b88.addActionListener((ActionListener) this);
        b89.addActionListener((ActionListener) this);
        b90.addActionListener((ActionListener) this);
        b91.addActionListener((ActionListener) this);
        b92.addActionListener((ActionListener) this);
        b93.addActionListener((ActionListener) this);
        b94.addActionListener((ActionListener) this);
        b95.addActionListener((ActionListener) this);
        b96.addActionListener((ActionListener) this);
        b97.addActionListener((ActionListener) this);
        b98.addActionListener((ActionListener) this);
        b99.addActionListener((ActionListener) this);

        b00.addMouseListener(ml);
        b01.addMouseListener(ml);
        b02.addMouseListener(ml);
        b03.addMouseListener(ml);
        b04.addMouseListener(ml);
        b05.addMouseListener(ml);
        b06.addMouseListener(ml);
        b07.addMouseListener(ml);
        b08.addMouseListener(ml);
        b09.addMouseListener(ml);
        b10.addMouseListener(ml);
        b11.addMouseListener(ml);
        b12.addMouseListener(ml);
        b13.addMouseListener(ml);
        b14.addMouseListener(ml);
        b15.addMouseListener(ml);
        b16.addMouseListener(ml);
        b17.addMouseListener(ml);
        b18.addMouseListener(ml);
        b19.addMouseListener(ml);
        b20.addMouseListener(ml);
        b21.addMouseListener(ml);
        b22.addMouseListener(ml);
        b23.addMouseListener(ml);
        b24.addMouseListener(ml);
        b25.addMouseListener(ml);
        b26.addMouseListener(ml);
        b27.addMouseListener(ml);
        b28.addMouseListener(ml);
        b29.addMouseListener(ml);
        b30.addMouseListener(ml);
        b31.addMouseListener(ml);
        b32.addMouseListener(ml);
        b33.addMouseListener(ml);
        b34.addMouseListener(ml);
        b35.addMouseListener(ml);
        b36.addMouseListener(ml);
        b37.addMouseListener(ml);
        b38.addMouseListener(ml);
        b39.addMouseListener(ml);
        b40.addMouseListener(ml);
        b41.addMouseListener(ml);
        b42.addMouseListener(ml);
        b43.addMouseListener(ml);
        b44.addMouseListener(ml);
        b45.addMouseListener(ml);
        b46.addMouseListener(ml);
        b47.addMouseListener(ml);
        b48.addMouseListener(ml);
        b49.addMouseListener(ml);
        b50.addMouseListener(ml);
        b51.addMouseListener(ml);
        b52.addMouseListener(ml);
        b53.addMouseListener(ml);
        b54.addMouseListener(ml);
        b55.addMouseListener(ml);
        b56.addMouseListener(ml);
        b57.addMouseListener(ml);
        b58.addMouseListener(ml);
        b59.addMouseListener(ml);
        b60.addMouseListener(ml);
        b61.addMouseListener(ml);
        b62.addMouseListener(ml);
        b63.addMouseListener(ml);
        b64.addMouseListener(ml);
        b65.addMouseListener(ml);
        b66.addMouseListener(ml);
        b67.addMouseListener(ml);
        b68.addMouseListener(ml);
        b69.addMouseListener(ml);
        b70.addMouseListener(ml);
        b71.addMouseListener(ml);
        b72.addMouseListener(ml);
        b73.addMouseListener(ml);
        b74.addMouseListener(ml);
        b75.addMouseListener(ml);
        b76.addMouseListener(ml);
        b77.addMouseListener(ml);
        b78.addMouseListener(ml);
        b79.addMouseListener(ml);
        b80.addMouseListener(ml);
        b81.addMouseListener(ml);
        b82.addMouseListener(ml);
        b83.addMouseListener(ml);
        b84.addMouseListener(ml);
        b85.addMouseListener(ml);
        b86.addMouseListener(ml);
        b87.addMouseListener(ml);
        b88.addMouseListener(ml);
        b89.addMouseListener(ml);
        b90.addMouseListener(ml);
        b91.addMouseListener(ml);
        b92.addMouseListener(ml);
        b93.addMouseListener(ml);
        b94.addMouseListener(ml);
        b95.addMouseListener(ml);
        b96.addMouseListener(ml);
        b97.addMouseListener(ml);
        b98.addMouseListener(ml);
        b99.addMouseListener(ml);

        rboot.addActionListener((ActionListener) this);
        rkreuzer.addActionListener((ActionListener) this);
        rzerstoerer.addActionListener((ActionListener) this);
        rflugzeugtraeger.addActionListener((ActionListener) this);
        chorizontal.addActionListener((ActionListener) this);

        bsetzen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                // schiff gesetzt an position des ausgewählten buttons
                if (auswahlmodus) {
                    auswahlmodus = false; // spielmodus
                    for (int i = 0; i < posButton.length; i++) {
                        for (int j = 0; j < posButton[0].length; j++) {
                            posButton[i][j].setEnabled(true);
                            posButton[i][j].setForeground(oldcolor);
                            posButton[i][j].setBackground(oldcolor);
                        }
                    }
                    // speicher positionen in DB
                    Datenbank db = new Datenbank();
                    try {
                        db.getConnection();
                        db.speicherSpieler(spieler);
                        db.speicherKarte(spieler);

                        // verbindung schließen
                        db.closeConn();
                    } catch (SQLException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                } else {
                    auswahlmodus = true;
                }
            }
        });
    }

    MouseListener ml = new MouseListener() {
        @Override
        public void mouseReleased(MouseEvent e) {    }
        @Override
        public void mousePressed(MouseEvent e) {    }
        @Override
        public void mouseExited(MouseEvent e) {
            // reset
            Object o = e.getSource();
            JButton bt;
            if (auswahlmodus) {
                int x;
                int y;
                boolean horizontal = chorizontal.isSelected();

                if (o instanceof JButton) {
                    bt = (JButton) o;
                    if (bt != null && bt.isEnabled()) {
                        String bname = bt.getText();
                        x = Character.getNumericValue(bname.charAt(0));
                        y = Character.getNumericValue(bname.charAt(1));
                        // Schiffart
                        if (rboot.isSelected()) {
                            Schiffe.markieren(posButton, oldcolor,0, x, y, horizontal);
                        } else if (rkreuzer.isSelected()) {
                            Schiffe.markieren(posButton, oldcolor, 1, x, y, horizontal);
                        } else if (rzerstoerer.isSelected()) {
                            Schiffe.markieren(posButton, oldcolor, 2, x, y, horizontal);
                        } else if (rflugzeugtraeger.isSelected()) {
                            Schiffe.markieren(posButton, oldcolor, 3, x, y, horizontal);
                        }
                    }
                }
            }
        }
        @Override
        public void mouseEntered(MouseEvent e) {
            Object o = e.getSource();
            JButton bt;
            if (auswahlmodus) { // setzmodus
                int x;
                int y;
                boolean horizontal = chorizontal.isSelected();
                if (o instanceof JButton) {
                    bt = (JButton) o;
                    if (bt != null && bt.isEnabled()) {
                        String bname = bt.getText();
                        x = Character.getNumericValue(bname.charAt(0));
                        y = Character.getNumericValue(bname.charAt(1));
                        // Schiffart
                        if (rboot.isSelected()) {
                            Schiffe.markieren(posButton, Spiellayout.markierColor,0, x, y, horizontal);
                        } else if (rkreuzer.isSelected()) {
                            Schiffe.markieren(posButton, Spiellayout.markierColor, 1, x, y, horizontal);
                        } else if (rzerstoerer.isSelected()) {
                            Schiffe.markieren(posButton, Spiellayout.markierColor, 2, x, y, horizontal);
                        } else if (rflugzeugtraeger.isSelected()) {
                            Schiffe.markieren(posButton, Spiellayout.markierColor, 3, x, y, horizontal);
                        }
                    }
                }
            }

        }
        @Override
        public void mouseClicked(MouseEvent e) {    }
    };

    public void actionPerformed(ActionEvent actionEvent) {
        Object o = actionEvent.getSource();
        JButton bt;
        if (auswahlmodus) { // setzmodus
            int nr;
            int x;
            int y;
            boolean horizontal = chorizontal.isSelected();

            if (o instanceof JButton) {
                bt = (JButton) o;
                if (bt != null && bt.isEnabled()) {
                    String bname = bt.getText();
                    x = Character.getNumericValue(bname.charAt(0));
                    y = Character.getNumericValue(bname.charAt(1));
                    // Schiffart
                    if (rboot.isSelected()) {
                        if (bootNr < bootMax) {
                            nr = bootNr++;
                            if (!Spieler.schiffeSet(posButton, Spiellayout.setzColor, spieler, nr, x, y, horizontal)) {
                                bootNr--;
                                // schiff wurde nicht gesetzt, falsche position
                            }
                        } else {
                            System.out.println("Keine Boote mehr verfügbar");
                        }
                    } else if (rkreuzer.isSelected()) {
                        if (kreuzerNr < kreuzerMax) {
                            nr = kreuzerNr++;
                            if (!Spieler.schiffeSet(posButton, Spiellayout.setzColor, spieler, nr, x, y, horizontal)) {
                                kreuzerNr--;
                                // schiff wurde nicht gesetzt, falsche position
                            }
                        } else {
                            System.out.println("Keine Kreuzer mehr verfügbar");
                        }
                    } else if (rzerstoerer.isSelected()) {
                        if (zerstoererNr < zerstoerertMax) {
                            nr = zerstoererNr++;
                            if (!Spieler.schiffeSet(posButton, Spiellayout.setzColor, spieler, nr, x, y, horizontal)) {
                                zerstoererNr--;
                                // schiff wurde nicht gesetzt, falsche position
                            }
                        } else {
                            System.out.println("Keine Zerstörer mehr verfügbar");
                        }
                    } else if (rflugzeugtraeger.isSelected()) {
                        if (flugzeugtraegerNr < flugzeugtraegerMax) {
                            nr = flugzeugtraegerNr++;
                            if (!Spieler.schiffeSet(posButton, Spiellayout.setzColor, spieler, nr, x, y, horizontal)) {
                                flugzeugtraegerNr--;
                                // schiff wurde nicht gesetzt, falsche position
                            }
                        } else {
                            System.out.println("Keine Flugzeugträger mehr verfügbar");
                        }
                    }
                }
            }
        }

        if (!auswahlmodus) { // spielmodus
            if (o instanceof JButton) {
                bt = (JButton) o;
                if (bt != null) {
                    String bname = bt.getText();
                    int x = Character.getNumericValue(bname.charAt(0));
                    int y = Character.getNumericValue(bname.charAt(1));
                    String pos = ((char) (y + 1 + 64)) + "" + (x + 1);
                    System.out.println("Es wurde auf " + pos + " geschossen");
                    bt.setEnabled(false);
                    bt.setOpaque(true);
                    bt.setBackground(Spiellayout.verfehltColor);
                    bt.setForeground(Spiellayout.verfehltColor);
                    //bt.setText("");
                    // Achtung postion aus text dadurch nicht mehr möglich
                    bt.setToolTipText("Verfehlt");
                    spieler.getKarte().getroffen(x,y,bt,spieler);
                }
            }
        }

        if (o instanceof JButton) {
            bt = (JButton) o;
            if (bt != null && bt.isEnabled()) {
                if (bootMax <= bootNr && kreuzerMax <= kreuzerNr && zerstoerertMax <= zerstoererNr && flugzeugtraegerMax <= flugzeugtraegerNr) {
                    // keine Schiffe mehr da
                    System.out.println("Alle Schiffe wurden bereits gesetzt. Spielmodus jetzt aktiv.");
                    auswahlmodus = false;

                    // speicher positionen in DB
                    Datenbank db = new Datenbank();
                    try {
                        db.getConnection();
                        db.speicherSpieler(spieler);
                        db.speicherKarte(spieler);

                        // verbindung schließen
                        db.closeConn();
                    } catch (SQLException | ClassNotFoundException e) {
                        e.printStackTrace();
                    }

                    for (int i = 0; i < posButton.length; i++) {
                        for (int j = 0; j < posButton[0].length; j++) {
                            posButton[i][j].setEnabled(true);
                            posButton[i][j].setForeground(oldcolor);
                            posButton[i][j].setBackground(oldcolor);
                        }
                    }
                }
            }
        }
    }
}
