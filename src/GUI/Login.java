package GUI;

import Spiel.Spieler;
import javax.swing.*;
import java.awt.event.*;

public class Login extends JFrame {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField username;
    private JPasswordField password;

    public Login() {
        setContentPane(contentPane);
        getRootPane().setDefaultButton(buttonOK);
        pack();
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLocationRelativeTo(null);

        username.addActionListener(al);
        password.addActionListener(al);
        buttonOK.addActionListener(al);
        buttonCancel.addActionListener(al);

    }
    

    ActionListener al = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent actionEvent) {
            Object o = actionEvent.getSource();
            if (o instanceof JButton) {
                JButton jb = (JButton) o;
                if (jb.getText().equals("Anmelden")) {
                    // login
                    String user = username.getText();
                    String pw = String.valueOf(password.getPassword());
                    // unsicher
                    login(user, pw);
                }
                if (jb.getText().equals("Abbruch")) {
                    // abbruch
                    dispose();
                    System.exit(0);
                }

            }
        }

    };

    private void login(String user, String pw) {
        // überprüfen ob in DB
        if (validPW(user,pw)) {
            System.out.println("Erfolgreich angemeldet als " + user);
            Spieler s = generiereSpieler(user, pw, true);
            // suche gegner
            Spieler gegner = sucheGegner();
            // starte Spiel
            Spiellayout sl = new Spiellayout(s);
        } else {
            System.out.println("Falscher Nutzername oder falsches Passwort (oder unbekannt)");
        }
    }

    private Spieler sucheGegner() {
        ///// genger suchen
        return new Spieler("Gegner","asdf");
    }

    private Spieler generiereSpieler(String name, String pw, boolean vorhanden) {
        // wenn in der DB hole Daten, sonst


        // erstelle Spieler anhand von Name und PW usw.
        return new Spieler(name, pw);

    }

    private boolean validPW(String user, String pw) {
        // hier DB abfrage
        if (user.equals("Manu") && pw.equals("1234")) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {
        Login dialog = new Login();
    }
}